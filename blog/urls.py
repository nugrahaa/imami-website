from django.urls import path
from django.views.generic.base import View
from . import views

app_name = "blog"

urlpatterns = [
    path("api", views.PostsList.as_view()),
    path("api/get/post", views.GetPost.as_view()),
]
