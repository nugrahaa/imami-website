from django.db import models
from django.contrib.auth.models import User


class TagsModel(models.Model):
    id = models.CharField(max_length=50, primary_key=True)

    def __str__(self):
        return self.id


class PostModel(models.Model):
    title = models.CharField(max_length=100)
    author = models.ForeignKey(
        User, on_delete=models.SET_DEFAULT, default="Deleted User"
    )
    content = models.TextField(max_length=10000)
    date_created = models.DateField(auto_now_add=True)
    tags = models.ManyToManyField(TagsModel)

    def __str__(self) -> str:
        return self.title
