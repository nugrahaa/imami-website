from django.contrib import admin
from .models import PostModel, TagsModel

admin.site.register(TagsModel)
admin.site.register(PostModel)
