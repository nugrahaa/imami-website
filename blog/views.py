from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import PostModel
from .serializers import PostSerializer


class PostsList(generics.ListAPIView):
    queryset = PostModel.objects.all()
    serializer_class = PostSerializer


class GetPost(APIView):
    serializer_class = PostSerializer

    def get(self, request, format=None):
        post_id = request.GET.get("id")
        if post_id != None:
            post = PostModel.objects.filter(id=post_id)
            if post.exists():
                data = PostSerializer(post[0]).data
                return Response(data, status=status.HTTP_200_OK)
            return Response(
                {
                    "Post Not Found": "Post instance with the specified ID does not exist in the database."
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        return Response(
            {
                "Bad Request": "ID parameter is not found in the request. Make sure add ID parameter such as: 'https://imamiui.herokuapp.com/api/get/post?id=1'."
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
